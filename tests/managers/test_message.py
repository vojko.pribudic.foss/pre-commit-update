from src.pre_commit_update.managers import MessageManager
from src.pre_commit_update.utils import get_color


def test_message_manager_to_update() -> None:
    name: str = "test"
    current_version: str = "0.0.1"
    latest_version: str = "0.0.2"
    message_manager: MessageManager = MessageManager()
    message_manager.add_update_message(name, current_version, latest_version)
    to_update_message: str = (
        f"{name} - {get_color(current_version, 'yellow')} -> {get_color(latest_version, 'red')}"
    )
    assert len(message_manager.update.messages) == 1
    assert message_manager.update.messages[0].text == to_update_message


def test_message_manager_no_update() -> None:
    name: str = "test"
    version: str = "0.0.1"
    message_manager: MessageManager = MessageManager()
    message_manager.add_no_update_message(name, version)
    no_update_message: str = f"{name} - {get_color(version, 'green')}"
    assert len(message_manager.no_update.messages) == 1
    assert message_manager.no_update.messages[0].text == no_update_message


def test_message_manager_excluded() -> None:
    name: str = "test"
    version: str = "0.0.1"
    message_manager: MessageManager = MessageManager()
    message_manager.add_exclude_message(name, version)
    excluded_message: str = f"{name} - {get_color(version, 'magenta')}"
    assert len(message_manager.exclude.messages) == 1
    assert message_manager.exclude.messages[0].text == excluded_message


def test_message_manager_kept() -> None:
    name: str = "test"
    current_version: str = "0.0.1"
    latest_version: str = "0.0.2"
    message_manager: MessageManager = MessageManager()
    message_manager.add_keep_message(name, current_version, current_version)
    message_manager.add_keep_message(name, current_version, latest_version)
    kept_message_same_version: str = f"{name} - {get_color(current_version, 'blue')}"
    kept_message_different_version: str = (
        f"{name} - {get_color(f'{current_version} -> {latest_version}', 'blue')}"
    )
    assert len(message_manager.keep.messages) == 2
    assert message_manager.keep.messages[0].text == kept_message_same_version
    assert message_manager.keep.messages[1].text == kept_message_different_version


def test_message_manager_warning() -> None:
    name: str = "test"
    reason: str = "some_reason"
    message_manager: MessageManager = MessageManager()
    message_manager.add_warning_message(name, reason)
    warning_message: str = f"{name} - {get_color(reason, 'yellow')}"
    assert len(message_manager.warning.messages) == 1
    assert message_manager.warning.messages[0].text == warning_message
