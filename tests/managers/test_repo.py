from typing import Union
from unittest.mock import patch

from src.pre_commit_update.managers import MessageManager, RepoManager

REPOS_DATA: list[dict[str, Union[str, list]]] = [
    {
        "repo": "https://gitlab.com/vojko.pribudic.foss/pre-commit-update",
        "rev": "v0.5.0",
        "hooks": [{"id": "pre-commit-update"}],
    },
    {
        "repo": "https://gitlab.com/non.existing/tag-prefix-hook",
        "rev": "version-0.4.0",
        "hooks": [{"id": "tag-prefix-hook"}],
    },
    {
        "repo": "https://gitlab.com/non.existing/tag-no-match-hook",
        "rev": "v0.4.0",
        "hooks": [{"id": "tag-no-match-hook"}],
    },
    {"repo": "local", "hooks": [{"id": "local-hook"}]},
    {
        "repo": "https://gitlab.com/non.important/excluded-hook",
        "rev": "v0.1.0",
        "hooks": [{"id": "excluded-hook"}],
    },
    {
        "repo": "https://gitlab.com/non.existing/kept-hook",
        "rev": "v0.1.0",
        "hooks": [{"id": "kept-hook"}],
    },
    {
        "repo": "https://some-invalid.com/hook",
        "rev": "v0.0.0",
        "hooks": [{"id": "invalid-hook"}],
    },
]

TAG_MAP: dict[str, list[str]] = {
    "https://gitlab.com/vojko.pribudic.foss/pre-commit-update": [
        "e155d9bf2c898b731dac079b9cc4844fbe8700d7\trefs/tags/version-invalid-0.1.0",
        "24f1ed432cb13408ebd1d77fd618c8322cff693d\trefs/tags/v0.5.0",
    ],
    "https://gitlab.com/non.existing/tag-prefix-hook": [
        "e155d9bf2c898b731dac079b9cc4844fbe8700d7\trefs/tags/version-0.5.0",
        "24f1ed432cb13408ebd1d77fd618c8322cff693d\trefs/tags/v0.5.0",
    ],
    "https://gitlab.com/non.existing/tag-no-match-hook": [
        "e155d9bf2c898b731dac079b9cc4844fbe8700d7\trefs/tags/no-match",
        "e155d9bf2c898b731dac079b9cc4844fbe8700d7\trefs/tags/no-match-v0.0a.23d",
    ],
    "https://gitlab.com/non.existing/kept-hook": [
        "24f1ed432cb13408ebd1d77fd618c8322cff693d\trefs/tags/v0.5.0",
    ],
}

HASH_MAP: dict[str, str] = {
    "https://gitlab.com/vojko.pribudic.foss/pre-commit-update": "24f1ed432cb13408ebd1d77fd618c8322cff693d",
    "https://gitlab.com/non.existing/tag-prefix-hook": "e155d9bf2c898b731dac079b9cc4844fbe8700d7",
    "https://gitlab.com/non.existing/tag-no-match-hook": "e155d9bf2c898b731dac079b9cc4844fbe8700d7",
    "https://gitlab.com/non.existing/kept-hook": "24f1ed432cb13408ebd1d77fd618c8322cff693d",
}


def _get_repo_tags_and_hashes(url: str) -> list:
    if url not in TAG_MAP:
        raise Exception
    return TAG_MAP[url]


def _get_repo_latest_hash(url: str) -> str:
    if url not in HASH_MAP:
        raise Exception
    return HASH_MAP[url]


@patch(
    "src.pre_commit_update.managers.repo.get_git_remote_tags_list",
    side_effect=_get_repo_tags_and_hashes,
)
@patch(
    "src.pre_commit_update.managers.repo.get_git_remote_latest_hash",
    side_effect=_get_repo_latest_hash,
)
def test_repo_manager(*_):
    message_manager: MessageManager = MessageManager()
    repo_manager: RepoManager = RepoManager(
        REPOS_DATA,
        False,
        10,
        ("excluded-hook",),
        ("kept-hook",),
        (),
        (("tag-prefix-hook", "version-"),),
    )
    repo_manager.get_updates(message_manager)
    assert repo_manager.repos_data == REPOS_DATA
