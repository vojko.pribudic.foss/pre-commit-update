import pytest

from src.pre_commit_update.managers import MessageManager, OptionManager


@pytest.mark.parametrize(
    "warnings, all_versions, exclude, keep, bleeding_edge, tag_prefix, repo_trims, res_warnings_len, res_exclude, res_keep, res_bleeding_edge, res_tag_prefix",
    [
        # Test with warnings=False, all_versions=False
        (
            False,
            False,
            ("black",),
            ("pylint",),
            ("isort",),
            (("pre-commit-update", "version"),),
            {"black", "pylint", "isort", "pre-commit-update"},
            0,
            ("black",),
            ("pylint",),
            ("isort",),
            (("pre-commit-update", "version"),),
        ),
        # Test with warnings=True, all_versions=False and invalid repo_trims
        (
            True,
            False,
            ("black", "blaack"),
            ("pylint", "pyylint"),
            ("isort", "issort"),
            (
                ("pre-commit-update", "version"),
                ("pre-commit-updaddte", "version"),
            ),
            {"black", "pylint", "isort", "pre-commit-update"},
            5,
            ("black",),
            ("pylint",),
            ("isort",),
            (("pre-commit-update", "version"),),
        ),
        # Test with warnings=True, all_versions=False and exclude wildcard
        (
            True,
            False,
            ("*", "black"),
            ("pylint",),
            ("isort",),
            (("pre-commit-update", "version"),),
            {"black", "pylint", "isort", "pre-commit-update"},
            3,
            ("*",),
            (),
            (),
            (("pre-commit-update", "version"),),
        ),
        # Test with warnings=True, all_versions=False and keep wildcard
        (
            True,
            False,
            ("black",),
            ("*", "pylint"),
            ("isort",),
            (("pre-commit-update", "version"),),
            {"black", "pylint", "isort", "pre-commit-update"},
            3,
            (),
            ("*",),
            ("isort",),
            (("pre-commit-update", "version"),),
        ),
        # Test with warnings=True, all_versions=True and bleeding_edge wildcard
        (
            True,
            True,
            ("black",),
            ("pylint",),
            ("*", "isort"),
            (("pre-commit-update", "version"),),
            {"black", "pylint", "isort", "pre-commit-update"},
            2,
            (),
            ("pylint",),
            ("*",),
            (("pre-commit-update", "version"),),
        ),
        # Test with warnings=True, all_versions=True and multiple wildcards
        (
            True,
            True,
            ("*",),
            ("*",),
            ("*", "isort"),
            (("pre-commit-update", "version"),),
            {"black", "pylint", "isort", "pre-commit-update"},
            1,
            ("*",),
            (),
            (),
            (("pre-commit-update", "version"),),
        ),
        # Test exclusive repo trims with warnings=True, all_versions=False
        (
            True,
            False,
            ("black", "isort"),
            ("pylint", "black"),
            ("isort", "pylint"),
            (("pre-commit-update", "version"),),
            {"black", "pylint", "isort", "pre-commit-update"},
            3,
            ("black", "isort"),
            ("pylint",),
            ("pylint",),
            (("pre-commit-update", "version"),),
        ),
        # Test duplicate repo trims with warnings=True, all_versions=False
        (
            True,
            False,
            ("black", "isort", "black"),
            ("pylint", "black", "pylint"),
            ("isort", "pylint", "isort"),
            (("pre-commit-update", "version"), ("pre-commit-update", "v")),
            {"black", "pylint", "isort", "pre-commit-update"},
            7,
            ("black", "isort"),
            ("pylint",),
            ("pylint",),
            (("pre-commit-update", "version"),),
        ),
    ],
)
def test_option_manager(
    warnings: bool,
    all_versions: bool,
    exclude: tuple,
    keep: tuple,
    bleeding_edge: tuple,
    tag_prefix: tuple,
    repo_trims: set,
    res_warnings_len: int,
    res_exclude: tuple,
    res_keep: tuple,
    res_bleeding_edge: tuple,
    res_tag_prefix: tuple,
) -> None:
    message_manager: MessageManager = MessageManager()
    option_manager: OptionManager = OptionManager(
        warnings=warnings,
        all_versions=all_versions,
        exclude=exclude,
        keep=keep,
        bleeding_edge=bleeding_edge,
        tag_prefix=tag_prefix,
        repo_trims=repo_trims,
    )
    option_manager.validate(message_manager)

    assert option_manager.exclude == res_exclude
    assert option_manager.keep == res_keep
    assert option_manager.bleeding_edge == res_bleeding_edge
    assert option_manager.tag_prefix == res_tag_prefix
    if warnings:
        assert len(message_manager.warning.messages) == res_warnings_len
