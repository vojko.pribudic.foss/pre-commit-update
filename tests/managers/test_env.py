import os

from src.pre_commit_update.managers import EnvManager


def test_env_manager() -> None:
    # Backup current state
    git_terminal_prompt: str = os.getenv("GIT_TERMINAL_PROMPT", "0")
    python_io_encoding: str = os.getenv("PYTHONIOENCODING", "UTF-8")
    python_utf_8: str = os.getenv("PYTHONUTF8", "1")
    env_manager: EnvManager = EnvManager()
    # Change the current state
    env_manager.setup()
    assert os.getenv("GIT_TERMINAL_PROMPT") == "0"
    assert os.getenv("PYTHONIOENCODING") == "UTF-8"
    assert os.getenv("PYTHONUTF8") == "1"
    # Restore and check against initial state
    env_manager.restore()
    assert os.getenv("GIT_TERMINAL_PROMPT") == git_terminal_prompt
    assert os.getenv("PYTHONIOENCODING") == python_io_encoding
    assert os.getenv("PYTHONUTF8") == python_utf_8
