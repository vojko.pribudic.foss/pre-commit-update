import os
import tempfile

from src.pre_commit_update.managers import YAMLManager


def test_yaml_manager() -> None:
    repo_url: str = "https://gitlab.com/vojko.pribudic.foss/pre-commit-update"
    repo_rev: str = "v0.3.3post1"
    yaml_data: str = f"""repos:
  - repo: {repo_url}
    rev: {repo_rev}
    hooks:
      - id: pre-commit-update"""

    with tempfile.TemporaryDirectory(dir="./tests") as temp_dir:
        file_path: str = os.path.join(temp_dir, "yaml_file.yaml")
        with open(file_path, "w") as f:
            f.write(yaml_data)
        yaml_manager: YAMLManager = YAMLManager(file_path)
        assert "repos" in yaml_manager.data
        assert len(yaml_manager.data["repos"]) == 1
        assert yaml_manager.data["repos"][0]["repo"] == repo_url
        assert yaml_manager.data["repos"][0]["rev"] == repo_rev

        yaml_manager.data = {"key": "value"}
        yaml_manager.dump()
        with open(file_path, "r") as f:
            assert f.read().strip() == "key: value"
