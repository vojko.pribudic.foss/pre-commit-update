import os
import tempfile

from src.pre_commit_update.utils import (
    get_converted_dict_values,
    get_converted_iterable,
    get_dict_diffs,
    get_toml_config,
    is_git_remote,
)


def test_is_git_remote() -> None:
    assert (
        is_git_remote("https://gitlab.com/vojko.pribudic.foss/pre-commit-update")
        == True
    )
    assert is_git_remote("git@github.com:pre-commit/pre-commit-hooks.git") == True
    assert is_git_remote("local") == False
    assert is_git_remote("meta") == False


def test_toml_config() -> None:
    # Create a new temp dir to hold the toml file
    with tempfile.TemporaryDirectory(dir="./tests") as temp_dir:
        file_path: str = os.path.join(temp_dir, "pyproject.toml")
        # Write the toml config data
        with open(file_path, "w") as f:
            f.write(
                """[tool.pre-commit-update]
                dry_run = true
                all_versions = false
                verbose = true
                warnings = false
                preview = false"""
            )
        # Test the toml config -> default config for verbose should be overwritten
        config: dict = get_toml_config({"verbose": False}, file_path)
        assert config["dry_run"] == True
        assert config["all_versions"] == False
        assert config["verbose"] == True
        assert config["warnings"] == False
        assert config["preview"] == False

    # Test the exception in case of missing toml config
    with tempfile.TemporaryDirectory(dir="./tests") as temp_dir:
        file_path: str = os.path.join(temp_dir, "pyproject.toml")
        # Write some toml config but not for [tool.pre-commit-update]
        with open(file_path, "w") as f:
            f.write(
                """[tool.poetry]
            name = 'pre-commit-update'"""
            )
        # Final config should be equal to the default one
        default_config: dict = {"verbose": True, "preview": False}
        assert get_toml_config(default_config, file_path) == default_config


def test_dict_diffs() -> None:
    dict_one: dict = {"a": 1, "b": 2, "c": 3}
    dict_two: dict = {"a": 1, "b": 3, "c": 2}
    expected_two_diff_one: dict = {"b": 3, "c": 2}
    expected_one_diff_two: dict = {"b": 2, "c": 3}
    assert get_dict_diffs(dict_one, dict_two) == expected_two_diff_one
    assert get_dict_diffs(dict_two, dict_one) == expected_one_diff_two


def test_converted_iterable() -> None:
    list_iterable: list = [1, 2, 3, [4, 5]]
    tuple_iterable: tuple = (1, 2, 3, (4, 5))
    assert get_converted_iterable(list_iterable, tuple) == tuple_iterable
    assert get_converted_iterable(tuple_iterable, list) == list_iterable


def test_converted_dict_values() -> None:
    dct: dict = {"a": (4, 5, 6), "b": {"1": (1, 2, 3)}}
    expected: dict = {"a": [4, 5, 6], "b": {"1": [1, 2, 3]}}
    assert get_converted_dict_values(dct) == expected
