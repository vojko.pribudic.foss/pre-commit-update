from typing import Optional

import pytest

from src.pre_commit_update.repo import Repo


def _get_repo_data(rev: str) -> dict:
    return {
        "repo": "https://gitlab.com/vojko.pribudic.foss/pre-commit-update",
        "rev": rev,
        "hooks": [{"id": "pre-commit-update"}],
        "trim": "pre-commit-update",
    }


def _get_tags_and_hashes(
    latest_tag: Optional[str] = None,
    latest_hash: Optional[str] = None,
    empty: bool = False,
) -> dict:
    if empty:
        return {}
    tags_and_hashes: dict = {
        "v0.5.0": "64835d09a5762654d6528eb90175f520eb96ddf2",
        "v0.4.0post1": "fad5b1039a2768d4ea63f53e4d9fd52027f13c92",
        "v0.4.0": "e0980e5224db89189260ff7e067b7066ffbbcf4f",
    }
    return (
        {**{latest_tag: latest_hash}, **tags_and_hashes}
        if latest_tag and latest_hash
        else tags_and_hashes
    )


@pytest.mark.parametrize(
    "repo_data, tags_and_hashes, latest_hash, all_versions, bleeding_edge, has_tags_and_hashes, latest_version",
    [
        # Test upgrade from "v0.3.3post1" to "v0.5.0"
        (
            _get_repo_data("v0.3.3post1"),
            _get_tags_and_hashes(),
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            True,
            "v0.5.0",
        ),
        # Test fallback to current version due to no tags/hashes
        (
            _get_repo_data("v0.3.3post1"),
            {},
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            False,
            "v0.3.3post1",
        ),
        # Test fallback to current version due to invalid tag versions
        (
            _get_repo_data("v0.3.3post1"),
            {"active": True, "local": False},
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            True,
            "v0.3.3post1",
        ),
        # Test fallback to current version due to invalid repo rev
        (
            _get_repo_data("something0.1.1"),
            _get_tags_and_hashes(),
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            True,
            "something0.1.1",
        ),
        # Test fallback to current version due to latest version hash matching latest_hash
        (
            _get_repo_data("64835d09a5762654d6528eb90175f520eb96ddf2"),
            _get_tags_and_hashes(),
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            True,
            "64835d09a5762654d6528eb90175f520eb96ddf2",
        ),
        # Test upgrade from "fad5b1039a2768d4ea63f53e4d9fd52027f13c92" to "64835d09a5762654d6528eb90175f520eb96ddf2"
        (
            _get_repo_data("fad5b1039a2768d4ea63f53e4d9fd52027f13c92"),
            _get_tags_and_hashes(),
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            True,
            "64835d09a5762654d6528eb90175f520eb96ddf2",
        ),
        # Test upgrade with all_versions=True -> "da39a3ee5e6b4b0d3255bfef95601890afd80709"
        (
            _get_repo_data("fad5b1039a2768d4ea63f53e4d9fd52027f13c92"),
            _get_tags_and_hashes(
                "v0.5.1a1", "da39a3ee5e6b4b0d3255bfef95601890afd80709"
            ),
            "da39a3ee5e6b4b0d3255bfef95601890afd80709",
            True,
            False,
            True,
            "da39a3ee5e6b4b0d3255bfef95601890afd80709",
        ),
        # Test upgrade with all_versions=False -> "64835d09a5762654d6528eb90175f520eb96ddf2"
        (
            _get_repo_data("fad5b1039a2768d4ea63f53e4d9fd52027f13c92"),
            _get_tags_and_hashes(
                "v0.5.1a1", "da39a3ee5e6b4b0d3255bfef95601890afd80709"
            ),
            "da39a3ee5e6b4b0d3255bfef95601890afd80709",
            False,
            False,
            True,
            "64835d09a5762654d6528eb90175f520eb96ddf2",
        ),
        # Test upgrade with bleeding_edge=True, version to hash -> "aa45899f8d128a865dbbd2fa10eea60aa9c85143"
        (
            _get_repo_data("v0.3.3post1"),
            _get_tags_and_hashes(),
            "aa45899f8d128a865dbbd2fa10eea60aa9c85143",
            False,
            True,
            True,
            "aa45899f8d128a865dbbd2fa10eea60aa9c85143",
        ),
        # Test upgrade with bleeding_edge=True, hash to hash -> "aa45899f8d128a865dbbd2fa10eea60aa9c85143"
        (
            _get_repo_data("fad5b1039a2768d4ea63f53e4d9fd52027f13c92"),
            _get_tags_and_hashes(),
            "aa45899f8d128a865dbbd2fa10eea60aa9c85143",
            False,
            True,
            True,
            "aa45899f8d128a865dbbd2fa10eea60aa9c85143",
        ),
        # Test upgrade from "" to "64835d09a5762654d6528eb90175f520eb96ddf2"
        (
            _get_repo_data(""),
            _get_tags_and_hashes(empty=True),
            "64835d09a5762654d6528eb90175f520eb96ddf2",
            False,
            False,
            False,
            "64835d09a5762654d6528eb90175f520eb96ddf2",
        ),
    ],
)
def test_repo(
    repo_data: dict,
    tags_and_hashes: dict,
    latest_hash: Optional[str],
    all_versions: bool,
    bleeding_edge: bool,
    has_tags_and_hashes: bool,
    latest_version: str,
) -> None:
    repo: Repo = Repo(
        repo=repo_data,
        tags_and_hashes=tags_and_hashes,
        latest_hash=latest_hash,
        all_versions=all_versions,
        bleeding_edge=bleeding_edge,
    )
    assert repo.trim == repo_data["trim"]
    assert repo.current_version == repo_data["rev"]
    assert repo.has_tags_and_hashes == has_tags_and_hashes
    assert repo.latest_version == latest_version
